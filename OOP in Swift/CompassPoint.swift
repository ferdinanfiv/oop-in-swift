//
//  CompassPoint.swift
//  OOP in Swift
//
//  Created by Ferdinan Linardi on 28/04/20.
//  Copyright © 2020 Ferdinan. All rights reserved.
//

import Foundation

enum CompassPoint {
    case north, south, east, west
    
    mutating func turnNorth() {
        self = .north
    }
}
