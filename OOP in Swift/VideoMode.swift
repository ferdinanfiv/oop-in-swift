//
//  VideoMode.swift
//  OOP in Swift
//
//  Created by Ferdinan Linardi on 28/04/20.
//  Copyright © 2020 Ferdinan. All rights reserved.
//

import Foundation

 class VideoMode {
        var resolution = Resolution()
        var interlaced = false
        var frameRate = 0.0
        var name: String?
    }
