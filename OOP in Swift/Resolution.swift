//
//  Resolution.swift
//  OOP in Swift
//
//  Created by Ferdinan Linardi on 28/04/20.
//  Copyright © 2020 Ferdinan. All rights reserved.
//

import Foundation

/**
 Ini adalah struct Resolution, digunakan bersamaan dengan class VideoMode
 */
struct Resolution {
    
var width = 0
var height = 0
}
