//
//  main.swift
//  OOP in Swift
//
//  Created by Ferdinan Linardi on 28/04/20.
//  Copyright © 2020 Ferdinan. All rights reserved.
//

import Foundation

let someResolution = Resolution()
let someVideoMode = VideoMode()

print("The width of someResolution is \(someResolution.width)")
print("The width of someVideoMode is \(someVideoMode.resolution.width) \n")

someVideoMode.resolution.width = 1280
print("The width of someVideoMode is now \(someVideoMode.resolution.width) \n")

let vga = Resolution(width: 640, height: 480)
let hd = Resolution(width: 1920, height: 1800)
var cinema = hd

cinema.width = 2048

print("Cinema is now \(cinema.width) pixels wide")
print("HD is still \(hd.width) pixels wide")

///
var currentDirection = CompassPoint.west
let rememberedDirection = currentDirection

currentDirection.turnNorth()

print("The current direction is \(currentDirection)")
print("The current direction is \(rememberedDirection)")

///

let tenEighty = VideoMode()

tenEighty.resolution = hd
tenEighty.interlaced = true
tenEighty.name = "1080i"
tenEighty.frameRate = 25.0

let alsoTenEighty = tenEighty
alsoTenEighty.frameRate = 30.0

print("The frameRate property of tenEighty is now \(tenEighty.frameRate)")


if tenEighty === alsoTenEighty {
    print("tenEighty and alsoTenEighty refers to the same VideoMode instance.")
}
